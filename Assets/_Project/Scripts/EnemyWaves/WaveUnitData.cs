﻿using System;
using UnityEngine;

[Serializable]
public class WaveUnitData
{
    [SerializeField]
    private ActorController enemyPrefab = null;
    [SerializeField]
    private int countForSpawn = 0;
    [SerializeField]
    private int scoreForKill = 0;

    public ActorController EnemyPrefab => enemyPrefab;
    public int CountForSpawn => countForSpawn;
    public int ScoreForKill => scoreForKill;
}
