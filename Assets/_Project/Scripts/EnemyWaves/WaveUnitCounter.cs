﻿public class WaveUnitCounter : ILimitedCounter<WaveUnitData>
{
    private int spawnedCount = 0;

    private WaveUnitData unitData = null;

    public WaveUnitCounter(WaveUnitData unitData)
    {
        this.unitData = unitData;
    }

    public WaveUnitData GetObject()
    {
        return unitData;
    }

    public void SetLimit(int limit)
    {
    }
    public void ResetCounter()
    {
        spawnedCount = 0;
    }
    public void IncreaseCounter()
    {
        spawnedCount++;
    }
    public bool IsLimitReached()
    {
        return spawnedCount >= unitData.CountForSpawn;
    }
}
