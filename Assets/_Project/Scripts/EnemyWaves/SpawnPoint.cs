﻿using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public WaveUnit SpawnUnit(WaveUnitData waveUnitData)
    {
        ActorController actor = Instantiate(waveUnitData.EnemyPrefab, transform.position, transform.rotation);
        WaveUnit waveUnit = actor.gameObject.AddComponent<WaveUnit>();
        waveUnit.Initialize(actor, waveUnitData);
        return waveUnit;
    }
}
