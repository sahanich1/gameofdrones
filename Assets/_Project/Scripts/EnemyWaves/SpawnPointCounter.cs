﻿public class SpawnPointCounter : ILimitedCounter<SpawnPoint>
{
    private SpawnPoint spawnPoint = null;
    public int MaxUnitCountForSpawn { get; private set; }
    public int CurrentSpawnedCount { get; private set; }

    public SpawnPointCounter(SpawnPoint spawnPoint)
    {
        this.spawnPoint = spawnPoint;
        CurrentSpawnedCount = 0;
        MaxUnitCountForSpawn = 0;
    }
    public void SetLimit(int maxUnitCountForSpawn)
    {
        MaxUnitCountForSpawn = maxUnitCountForSpawn;
    }

    public SpawnPoint GetObject()
    {
        return spawnPoint;
    }

    public void ResetCounter()
    {
        CurrentSpawnedCount = 0;
    }
    public void IncreaseCounter()
    {
        CurrentSpawnedCount++;
    }
    public bool IsLimitReached()
    {
        return CurrentSpawnedCount >= MaxUnitCountForSpawn;
    }
}
