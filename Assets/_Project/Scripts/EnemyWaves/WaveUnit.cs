﻿using System;
using UnityEngine;

public class WaveUnit : MonoBehaviour
{
    public WaveUnitData UnitData { get; private set; }

    private ActorController actor = null;

    public event Action<WaveUnitData> HasKilled;

    public void Initialize(ActorController actor, WaveUnitData unitData)
    {
        this.actor = actor;
        UnitData = unitData;

        actor.RegisterHealthChangedListener(OnActorHealthChanged);
    }

    public void OnActorHealthChanged()
    {
        if (!actor.IsAlive())
        {
            HasKilled?.Invoke(this.UnitData);
        }
    }

    public void RegisterKilledListener(Action<WaveUnitData> listener)
    {
        HasKilled += listener;
    }
    public void RemoveKilledListener(Action<WaveUnitData> listener)
    {
        HasKilled -= listener;
    }
}
