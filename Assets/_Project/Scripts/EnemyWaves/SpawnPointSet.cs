﻿using UnityEngine;

public class SpawnPointSet : MonoBehaviour
{
    [SerializeField]
    private SpawnPoint[] spawnPoints = null;

    public SpawnPoint[] SpawnPoints => spawnPoints;
}
