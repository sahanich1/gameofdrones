﻿using System;
using System.Collections;
using UnityEngine;

public class WaveController : MonoBehaviour
{
    [SerializeField]
    private EnemyWave[] enemyWaves = null;

    private int currentWaveIndex = -1;

    public EnemyWave CurrentWave { get; private set; }
    public int CurrentWaveIndex => currentWaveIndex;
    public int WaveCount => enemyWaves.Length;

    private event Action WaveCompleted;
    private event Action<WaveUnitData> UnitKilled;

    public void RunNextWave()
    {
        RunWave(currentWaveIndex + 1);
    }
    public void StopCurrentWave()
    {
        if (CurrentWave != null)
        {
            CurrentWave.Stop();
            CurrentWave = null;
        }
    }

    public void RegisterWaveCompletedListener(Action listener)
    {
        WaveCompleted += listener;
    }
    public void RemoveWaveCompletedListener(Action listener)
    {
        WaveCompleted -= listener;
    }

    public void RegisterUnitKilledListener(Action<WaveUnitData> listener)
    {
        UnitKilled += listener;
    }
    public void RemoveUnitKilledListener(Action<WaveUnitData> listener)
    {
        UnitKilled -= listener;
    }

    private void RunWave(int waveIndex)
    {
        if (waveIndex >= enemyWaves.Length)
        {
            return;
        }
        currentWaveIndex = waveIndex;
        CurrentWave = enemyWaves[currentWaveIndex];
        CurrentWave.RegisterUnitKilledListener(OnWaveUnitKilled);
        CurrentWave.RegisterWaveCompletedListener(OnWaveCompleted);
        CurrentWave.Run();
    }

    private void OnWaveCompleted()
    {
        WaveCompleted?.Invoke();
    }

    private void OnWaveUnitKilled(WaveUnitData waveUnitData)
    {
        UnitKilled?.Invoke(waveUnitData);
    }

}
