using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyWave : MonoBehaviour
{
    [SerializeField]
    private SpawnPointSet spawnPointSet = null;
    [SerializeField]
    private WaveUnitData[] waveUnitsData = null;
    [SerializeField]
    private int maxUnitCountOnMap = 0;
    [SerializeField]
    private float spawnDelay = 0;

    private System.Random waveRandom = new System.Random();
    private Coroutine unitSpawningCoroutine = null;

    private int currentUnitCountOnMap = 0;
    private int totalCountForSpawn = 0;
    private int totalKilledCount = 0;

    private event Action WaveStarted;
    private event Action WaveCompleted;
    private event Action<WaveUnitData> UnitKilled;

    public void RegisterWaveStartedListener(Action listener)
    {
        WaveStarted += listener;
    }
    public void RemoveWaveStartedListener(Action listener)
    {
        WaveStarted -= listener;
    }
    public void RegisterWaveCompletedListener(Action listener)
    {
        WaveCompleted += listener;
    }
    public void RemoveWaveCompletedListener(Action listener)
    {
        WaveCompleted -= listener;
    }
    public void RegisterUnitKilledListener(Action<WaveUnitData> listener)
    {
        UnitKilled += listener;
    }
    public void RemoveUnitKilledListener(Action<WaveUnitData> listener)
    {
        UnitKilled -= listener;
    }

    public void Run()
    {
        Stop();
        WaveStarted?.Invoke();

        List<ILimitedCounter<SpawnPoint>> activeSpawnPoints = new List<ILimitedCounter<SpawnPoint>>();
        List<WaveUnitData> singleWaveUnitsForSpawn = new List<WaveUnitData>();
        foreach (var item in waveUnitsData)
        {
            for (int i = 0; i < item.CountForSpawn; i++)
            {
                singleWaveUnitsForSpawn.Add(item);
            }
        }
        
        totalCountForSpawn = singleWaveUnitsForSpawn.Count;
        
        if (totalCountForSpawn <= 0)
        {
            WaveCompleted?.Invoke();
            return;
        }

        CommonFunctions.ShuffleList(ref singleWaveUnitsForSpawn);

        totalKilledCount = 0;
        currentUnitCountOnMap = 0;

        foreach (var item in spawnPointSet.SpawnPoints)
        {
            activeSpawnPoints.Add(new SpawnPointCounter(item));
        }

        int remainingCountForDistribute = totalCountForSpawn;
        for (int i = 0; i < activeSpawnPoints.Count; i++)
        {
            int countForSpawnPoint = remainingCountForDistribute / (activeSpawnPoints.Count - i);
            activeSpawnPoints[i].SetLimit(countForSpawnPoint);
            remainingCountForDistribute -= countForSpawnPoint;
        }

        unitSpawningCoroutine = StartCoroutine(PerformUnitSpawning(activeSpawnPoints, singleWaveUnitsForSpawn));       
    }

    public void Stop()
    {
        if (unitSpawningCoroutine != null)
        {
            StopCoroutine(unitSpawningCoroutine);
            unitSpawningCoroutine = null;
        }
    }

    private IEnumerator PerformUnitSpawning(List<ILimitedCounter<SpawnPoint>> activeSpawnPoints,
        List<WaveUnitData> singleWaveUnitsForSpawn)
    {
        int spawnedCount = 0;
        while (spawnedCount < totalCountForSpawn)
        {
                       
            if (currentUnitCountOnMap >= maxUnitCountOnMap)
            {
                yield return new WaitForSeconds(1);
                continue;
            }

            CommonFunctions.RemoveItemsReachedLimit(activeSpawnPoints);  

            int randomIndex = waveRandom.Next(activeSpawnPoints.Count);
            SpawnPoint spawnPoint = activeSpawnPoints[randomIndex].GetObject();
            activeSpawnPoints[randomIndex].IncreaseCounter();

            WaveUnitData waveUnitData = singleWaveUnitsForSpawn[0];
            singleWaveUnitsForSpawn.RemoveAt(0);

            WaveUnit unit = spawnPoint.SpawnUnit(waveUnitData);

            unit.RegisterKilledListener(OnWaveUnitKilled);
            
            spawnedCount++;
            currentUnitCountOnMap++;

            yield return new WaitForSeconds(spawnDelay);
        }

        unitSpawningCoroutine = null;
    }

    private void OnWaveUnitKilled(WaveUnitData unitData)
    {
        currentUnitCountOnMap--;
        totalKilledCount++;
        UnitKilled?.Invoke(unitData);
        if (totalKilledCount >= totalCountForSpawn)
        {
            WaveCompleted?.Invoke();
        }
    }

}
