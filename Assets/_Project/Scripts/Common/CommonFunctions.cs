﻿using System.Collections.Generic;
using System.Linq;

public static class CommonFunctions
{
    private static System.Random random = new System.Random();

    public static bool GetInterface<T>(in object obj, out T objInterface) where T : class
    {
        if (obj.GetType().GetInterfaces().Contains(typeof(T)))
        {
            objInterface = obj as T;
            return true;
        }
        objInterface = default;
        return false;
    }

    public static void ShuffleArray<T>(ref T[] array)
    {
        List<T> list = array.ToList();
        ShuffleList(ref list);
        array = list.ToArray();
    }

    public static void ShuffleList<T>(ref List<T> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            var randomIndex = random.Next(i, list.Count);
            var tempItem = list[i];
            list[i] = list[randomIndex];
            list[randomIndex] = tempItem;
        }
    }

    public static void RemoveItemsReachedLimit<T>(List<ILimitedCounter<T>> list)
    {
        for (int i = list.Count - 1; i >= 0; i--)
        {
            if (list[i].IsLimitReached())
            {
                list.RemoveAt(i);
            }
        }
    }
}
