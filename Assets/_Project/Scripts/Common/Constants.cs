using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public static class Messages
    {
        public const string NoPrefab = "Can't initialize prefab";
        public const string WaveCompleted = "Wave {0} completed!";
        public const string Wave = "Wave";
        public const string Fight = "Fight!";
    }
    public static class UI
    {
        public const string ScoreUnit = "cr.";
    }
    public static class Control
    {
        public const string Horizontal = "Horizontal";
        public const string Vertical = "Vertical";
    }
    public static class AnimatorParameters
    {
        public const string DeathTrigger = "DeathTrigger";
        public const string VelocityMultiplier = "VelocityMultiplier";
        public const string ForwardVelocity = "VelocityZ";
        public const string SideVelocity = "VelocityX";
        public const string Moving = "Moving";
        public const string Attacking = "Attacking";
        public const string Reloading = "Reloading";
    }
    public static class AnimatorLayers
    {
        public const string UpperBody = "UpperBody Layer";
    }
}
