using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEnemyControl : MonoBehaviour
{
    private const float destinationPositionAccuracy = 0.1f;

    [SerializeField]
    private ActorController target = null;
    [SerializeField]
    private float timeToNextPointCalculationMin = 1;
    [SerializeField]
    private float timeToNextPointCalculationMax = 3;

    private ActorController actor = null;

    private static readonly System.Random rand = new System.Random();

    private Vector3 currentMoveDirection = Vector3.zero;
    private Vector3 currentMovePoint = Vector3.zero;

    private void Start()
    {
        actor = GetComponent<ActorController>();
        if (target == null)
        {
            target = GameManager.Instance.Player;
        }
        actor.WeaponController.SetTarget(target);
        StartCoroutine(NextMovePointCalculation());
    }

    private void Update()
    {
        if (!actor.IsAlive() || !target.IsAlive())
        {
            actor.Rotate(transform.forward);
            actor.Move(Vector3.zero);
            enabled = false;
            return;
        }

        actor.Rotate(GetPositionToLookAt());

        HandleReload();
        HandleAttack();

        actor.Move(currentMoveDirection);
    }

    private void CalculateNextMovePoint()
    {
        float min = actor.WeaponController.AttackMinDistance;
        float max = actor.WeaponController.AttackMaxDistance;
        float moveX = min + (float)rand.NextDouble() * (max - min);
        float moveZ = min + (float)rand.NextDouble() * (max - min);

        // ��������� ����� ����������� ��� Y
        Vector3 targetPosition = target.GetPosition();
        targetPosition.y = transform.position.y;
        
        // ����� ����� �������� ����� ����������� �� ����, �.�. ��� ����� ��������� �� �� � �������� ������ ������� �����
        Vector3 ZDirection = (transform.position - targetPosition).normalized;
        Vector3 XDirection = new Vector3(-ZDirection.z, 0, ZDirection.x);

        // �������� ����� ���� ������, ���� ����� �� ����
        if (rand.Next(2) == 0)
        {
            XDirection = -XDirection;
        }
       
        Vector3 nextMovePoint = targetPosition + XDirection * moveX + ZDirection * moveZ;

        // ���� ���������� �� ���� �� ������������ ����� �������� ������ ������������ ��������� �����, 
        // ������������ ����� ���, ����� ���������� � �������� ������� �����
        if (Vector3.Distance(targetPosition, nextMovePoint) > actor.WeaponController.AttackMaxDistance)
        {
            nextMovePoint = (nextMovePoint - targetPosition).normalized *
                (min + (float)rand.NextDouble() * (max - min));
        }

        currentMovePoint = nextMovePoint;
        currentMoveDirection = (nextMovePoint - transform.position).normalized;
    }

    private IEnumerator NextMovePointCalculation()
    {
        float nextCalculationTime = 0;

        while (actor.IsAlive() && target != null && target.IsAlive())
        {
            if (Time.time > nextCalculationTime)
            {
                CalculateNextMovePoint();
                nextCalculationTime = Time.time + timeToNextPointCalculationMin +
                    (float)rand.NextDouble() * (timeToNextPointCalculationMax - timeToNextPointCalculationMin);
            }
            else if (currentMoveDirection != Vector3.zero && IsDestinationReached())
            {
                currentMoveDirection = Vector3.zero;
            }

            yield return null;
        }
    }

    private bool IsDestinationReached()
    {
        Vector3 distanceVector = currentMovePoint - transform.position;
        distanceVector.y = 0;
        return distanceVector.magnitude <= destinationPositionAccuracy;
    }

    private Vector3 GetPositionToLookAt()
    {
        return target.GetPosition();
    }

    private void HandleAttack()
    {
        actor.Attack(true);
    }
    private void HandleReload()
    {
        if (actor.WeaponController.GetWeaponStatus() == WeaponStatus.Unloaded)
        {
            actor.Reload();
        }
    }
}