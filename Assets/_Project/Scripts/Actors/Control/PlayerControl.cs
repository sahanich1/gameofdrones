using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(ActorController))]
public class PlayerControl : MonoBehaviour
{
    [SerializeField]
    private LayerMask mouseAimLayerMask;
    [SerializeField]
    private LayerMask enemyLayerMask;

    private ActorController playerActor = null;
    private Camera mainCamera = null;

    private void Start()
    {
        playerActor = GetComponent<ActorController>();
        // ����� ����� ��������� �� ����� ������������� �������� ��������������.
        playerActor.Reload();
        mainCamera = Camera.main;
    }

    private void Update()
    {
        if (!playerActor.IsAlive())
        {
            playerActor.Move(Vector3.zero);
            return;
        }

        playerActor.Rotate(GetPositionToLookAt());

        HandleAttack();
        HandleReload();

        Vector3 movementDirection = GetMoveDirection();
        playerActor.Move(movementDirection);
    }

    private Vector3 GetMoveDirection()
    {
        Vector3 movementVector = Vector3.zero;
        float horizontalMove = Input.GetAxis(Constants.Control.Horizontal);
        float verticalMove = Input.GetAxis(Constants.Control.Vertical);

        if (horizontalMove != 0 || verticalMove != 0)
        {
            movementVector = Vector3.ClampMagnitude(new Vector3(horizontalMove, 0, verticalMove), 1);
        }

        return movementVector;
    }

    private Vector3 GetPositionToLookAt()
    {
        Vector3 mousePosition = Input.mousePosition;
        Vector3 positionToLookAt = playerActor.GetPosition() + Vector3.forward;
        Ray cameraRay = mainCamera.ScreenPointToRay(mousePosition);

        if (Physics.Raycast(cameraRay, out var hitInfo, Mathf.Infinity, mouseAimLayerMask))
        {
            positionToLookAt = hitInfo.point;
        }

        bool targetSetted = false;
        if (Physics.Raycast(cameraRay, out hitInfo, Mathf.Infinity, enemyLayerMask))
        {
            IDamageable target = hitInfo.transform.GetComponent<IDamageable>();
            if (target != null && target.IsAlive())
            {
                playerActor.WeaponController.SetTarget(target);
                targetSetted = true;
                positionToLookAt.y = hitInfo.point.y;
            }           
        }

        if (!targetSetted)
        {
            playerActor.WeaponController.SetTarget(null);
        }

        return positionToLookAt;
    }

    private void HandleAttack()
    {
        if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            playerActor.Attack(false);
        }
    }
    private void HandleReload()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            playerActor.Reload();
        }
    }
}
