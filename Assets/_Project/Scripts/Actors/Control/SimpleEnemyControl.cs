using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ActorController))]
public class EnemyControl : MonoBehaviour
{
    [SerializeField]
    private ActorController target = null;

    private ActorController actor = null;

    private void Start()
    {
        actor = GetComponent<ActorController>();
        actor.WeaponController.SetTarget(target);
    }

    private void Update()
    {
        if (!actor.IsAlive() || !target.IsAlive())
        {
            return;
        }

        actor.Rotate(GetPositionToLookAt());

        HandleReload();
        HandleAttack();

        Vector3 movementDirection = GetMoveDirection();
        actor.Move(movementDirection);
    }

    private Vector3 GetMoveDirection()
    {
        Vector3 movementVector = Vector3.zero;

        AttackCheckResult checkTargetAttackDistanceResult = actor.WeaponController.CheckTargetAttackDistance();
        if (checkTargetAttackDistanceResult == AttackCheckResult.Ok)
        {
            return movementVector;
        }

        switch (checkTargetAttackDistanceResult)
        {
            case AttackCheckResult.TooShortDistance:
                movementVector = -transform.forward;
                break;
            case AttackCheckResult.TooLongDistance:
                movementVector = transform.forward;
                break;
            default:
                break;
        }

        return movementVector;
    }

    private Vector3 GetPositionToLookAt()
    {
        return target.GetPosition();
    }

    private void HandleAttack()
    {
        actor.Attack(true);
    }
    private void HandleReload()
    {
        if (actor.WeaponController.GetWeaponStatus() == WeaponStatus.Unloaded)
        {
            actor.Reload();
        }
    }
}
