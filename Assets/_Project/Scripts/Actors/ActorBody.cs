using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class ActorBody : MonoBehaviour
{
    private const float GroundCheckRayLength = 0.01f;
    private const float AccelerationOfGravity = 9.81f;

    protected CharacterController cc = null;
    protected Vector3 previousForwardDirection = Vector3.zero;

    public Vector3 GetPosition() => cc != null ? transform.position + cc.center : transform.position;
    public Vector3 GetForwardDirection() => transform.forward;
    
    public bool IsEnabled()
    {
        return cc != null && cc.enabled && isActiveAndEnabled;
    }
    public void SetEnabled(bool enabled)
    {
        this.enabled = enabled;
        cc.enabled = enabled;
    }

    public void Move(Vector3 movementVector)
    {
        if (cc == null)
        {
            return;
        }

        if (!IsGrounded())
        {
            movementVector += Vector3.down * AccelerationOfGravity;
        }

        if (movementVector != Vector3.zero)
        {
            cc.Move(movementVector * Time.deltaTime);
        }
    }

    public void Rotate(float rotationAngle)
    {
        transform.forward = Quaternion.Euler(0, rotationAngle, 0) * transform.forward;
    }

    protected virtual bool IsGrounded()
    {
        return Physics.Raycast(transform.position + cc.center * transform.localScale.y,
            Vector3.down, cc.bounds.extents.y + GroundCheckRayLength);
    }

    private void Awake()
    {
        cc = GetComponent<CharacterController>();
    }
}
