using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ActorBody))]
public class ActorController : MonoBehaviour, IDamageable
{
    [SerializeField]
    protected ActorModel actorModel = null;
    [SerializeField]
    protected WeaponController weaponController = null;
    [SerializeField]
    protected float afterDeathDestroyDelay = 5;

    protected ActorBody actorBody = null;

    protected Vector3 previousForwardDirection = Vector3.zero;

    public WeaponController WeaponController => weaponController;
    public float Health => actorModel.GetHealth();
    public float MaxHealth => actorModel.GetMaxHealth();
    public float Velocity => actorModel.GetVelocity();

    protected event Action DamageReceived;
    protected event Action<Vector3, Vector3> HasMoved;
    protected event Action<float> Attacking;
    protected event Action<float> Reloading;

    public Vector3 GetPosition() => actorBody.GetPosition();
    public bool IsAlive() => actorModel.IsAlive();

    public void ReceiveDamage(float damage)
    {
        float currentHealth = actorModel.GetHealth();
        if (currentHealth > 0)
        {
            actorModel.SetHealth(currentHealth - damage);
            DamageReceived?.Invoke();
        }
    }

    public void Move(Vector3 movementDirection)
    {
        if (!actorBody.IsEnabled())
        {
            return;
        }      

        if (weaponController.IsAttacking && !weaponController.AllowMovingAttack)
        {
            movementDirection = Vector3.zero;
        }

        Vector3 movementVector = movementDirection * actorModel.GetVelocity();

        actorBody.Move(movementVector);

        HasMoved?.Invoke(movementDirection, previousForwardDirection);
    }

    public void Rotate(Vector3 positionToLookAt)
    {
        previousForwardDirection = actorBody.GetForwardDirection();

        if (!actorBody.IsEnabled())
        {
            return;
        }

        if (weaponController.IsAttacking && !weaponController.AllowRotatingAttack)
        {
            return;
        }

        // ����������� ����� ���� ����� �� ��������� � ������ ����� (��������). 
        // � ���� ������ ����� ������ �� �����.
        // ������������ �������� ���� � ������ ����� �����.

        Vector3 targetPosition = Vector3.Scale(positionToLookAt, new Vector3(1, 0, 1));
        Transform weaponPoint = WeaponController.WeaponAttackPoint;
        Vector3 weaponPointPosition = Vector3.Scale(weaponPoint.position, new Vector3(1, 0, 1));
        Vector3 charPosition = Vector3.Scale(actorBody.GetPosition(), new Vector3(1, 0, 1));

        float distanceToWeaponPoint = Vector3.Distance(weaponPointPosition, charPosition);
        float distanceToAim = Vector3.Distance(targetPosition, charPosition);

        Vector3 previousTargetPosition = weaponPointPosition + weaponPoint.forward * (distanceToAim - distanceToWeaponPoint);
        float rotationAngle = Vector3.SignedAngle(previousTargetPosition - charPosition, targetPosition - charPosition, Vector3.up);

        actorBody.Rotate(rotationAngle);
    }

    public void Reload()
    {
        if (weaponController.IsBusy())
        {
            return;
        }

        // ��������, ��� ������� ������ - ������ �������� ���
        if (!CommonFunctions.GetInterface(weaponController, out IRangedWeaponController rangedWeaponController))
        {
            return;
        }

        rangedWeaponController.Reload();

        // ��������� ������ ������ ���� ���������� �� "�����������"
        if (!rangedWeaponController.IsReloading)
        {
            return;
        }

        Reloading?.Invoke(rangedWeaponController.ReloadTime);       
    }

    public void Attack(bool checkTarget)
    {
        if (weaponController.IsBusy())
        {
            return;
        }

        AttackCheckResult attackResult = checkTarget ? weaponController.AttackTarget() : weaponController.ForcedAttack();

        // ��������� ������ ������ ���� ���������� �� "�����"
        if (attackResult != AttackCheckResult.Ok)
        {
            return;
        }

        Attacking?.Invoke(weaponController.FullAttackTime);
    }

    public void RegisterHasMovedListener(Action<Vector3, Vector3> listener)
    {
        HasMoved += listener;
    }
    public void RemoveHasMovedListener(Action<Vector3, Vector3> listener)
    {
        HasMoved -= listener;
    }
    public void RegisterAttackingListener(Action<float> listener)
    {
        Attacking += listener;
    }
    public void RemoveAttackingListener(Action<float> listener)
    {
        Attacking -= listener;
    }
    public void RegisterReloadingListener(Action<float> listener)
    {
        Reloading += listener;
    }
    public void RemoveReloadingListener(Action<float> listener)
    {
        Reloading -= listener;
    }
    public void RegisterHealthChangedListener(Action listener)
    {
        actorModel.RegisterHealthChangedListener(listener);
    }
    public void RemoveHealthChangedListener(Action listener)
    {
        actorModel.RemoveHealthChangedListener(listener);
    }

    protected virtual void Awake()
    {
        actorBody = GetComponent<ActorBody>();
        actorModel = Instantiate(actorModel, transform);
    }

    protected virtual void Start()
    {
        previousForwardDirection = transform.forward;
        weaponController.SetOwner(gameObject);
        actorModel.RegisterHealthChangedListener(OnHealthChanged);
    }

    protected virtual void OnDestroy()
    {
        actorModel.RemoveHealthChangedListener(OnHealthChanged);
    }

    protected virtual void OnHealthChanged()
    {
        if (!actorModel.IsAlive())
        {
            actorBody.SetEnabled(false);
            weaponController.InterruptCurrentOperation();
            Destroy(gameObject, afterDeathDestroyDelay);
        }
    }
}
