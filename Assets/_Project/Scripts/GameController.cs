using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private ScreenController screenController = null;
    [SerializeField]
    private WaveController waveController = null;
    [SerializeField]
    private AmmoOffer ammoOffer = null;
    [SerializeField]
    private ScoreCounter scoreCounter = null;

    private ActorController playerActor = null;

    private void Start()
    {
        if (!waveController.isActiveAndEnabled)
        {
            return;
        }
        waveController.RegisterWaveCompletedListener(OnWaveCompleted);

        playerActor = GameManager.Instance.Player;
        playerActor.RegisterHealthChangedListener(OnPlayerHealthChanged);

        PrepareToStartNextWave();
    }
    private void OnDestroy()
    {
        waveController.RemoveWaveCompletedListener(OnWaveCompleted);
        playerActor.RemoveHealthChangedListener(OnPlayerHealthChanged);
    }

    private void PrepareToStartNextWave()
    {
        screenController.ShowWaveStartScreen(waveController.CurrentWaveIndex + 1, StartWave);
    }

    private void StartWave()
    {
        // ����� ��� ������� ������������ �� ����� �������
        if (playerActor == null || !playerActor.IsAlive())
        {
            return;
        }
        waveController.RunNextWave();        
        ammoOffer.RunOfferGenerator();
    }

    private void OnWaveCompleted()
    {
        ammoOffer.StopOfferGenerator();
        if (waveController.CurrentWaveIndex + 1 >= waveController.WaveCount)
        {
            screenController.ShowGameCompletedScreen(scoreCounter.TotalScoreCount);
            return;
        }

        screenController.ShowWaveCompletedScreen(waveController.CurrentWaveIndex, PrepareToStartNextWave);
    }

    private void OnPlayerHealthChanged()
    {
        if (!playerActor.IsAlive())
        {
            waveController.StopCurrentWave();
            ammoOffer.StopOfferGenerator();
            screenController.ShowGameOverScreen(scoreCounter.TotalScoreCount);
        }
    }
}
