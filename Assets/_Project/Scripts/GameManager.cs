using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private string gameSceneName = null;
    [SerializeField]
    private Transform ground = null;
    [SerializeField]
    private ActorController player = null;

    public float GroundTopY { get; private set; }
    public ActorController Player => player;

    public static GameManager Instance { get; private set; }

    public static void Quit()
    {
        Application.Quit();
    }
    public static void RunGame()
    {
        SceneManager.LoadScene(Instance.gameSceneName);
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        if (ground == null)
        {
            return;
        }

        GroundTopY = ground.position.y + ground.GetComponent<Collider>().bounds.size.y / 2;
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }
    
}
