using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyWave))]
public class EnemyWaveMusic : MonoBehaviour
{
    [SerializeField]
    private AudioSource waveMusic = null;
    [SerializeField]
    private AudioSource waveOnCompletedMusic = null;
    [SerializeField]
    private float volumeSmoothingTime = 5;
    [SerializeField]
    private float waveOnCompletedMusicDuration = 10;

    EnemyWave enemyWave = null;

    private void Awake()
    {
        enemyWave = GetComponent<EnemyWave>();
    }

    private void Start()
    {
        enemyWave.RegisterWaveStartedListener(OnWaveStarted);
        enemyWave.RegisterWaveCompletedListener(OnWaveCompleted);
    }

    private void OnWaveStarted()
    {
        if (waveMusic == null)
        {
            return;
        }
        PlayMusic(waveMusic);
    }
    private void OnWaveCompleted()
    {
        if (waveOnCompletedMusic == null)
        {
            StartCoroutine(StopMusicSmootly(waveMusic));
            return;
        }
        StopMusic(waveMusic);
        PlayMusic(waveOnCompletedMusic);
        StartCoroutine(StopMusicAfterTime(waveMusic, waveOnCompletedMusicDuration));
    }

    private IEnumerator StopMusicSmootly(AudioSource audioSource)
    {
        if (audioSource == null)
        {
            yield break;
        }

        if (volumeSmoothingTime <= 0)
        {
            StopMusic(audioSource);
            yield break;
        }

        float initialVolume = audioSource.volume;

        float zeroVolumeTime = Time.time + volumeSmoothingTime;
        float volumeChangeSpeed = audioSource.volume / volumeSmoothingTime;
       
        while (Time.time < zeroVolumeTime)
        {
            audioSource.volume -= Time.deltaTime * volumeChangeSpeed;
            yield return null;
        }

        StopMusic(audioSource);
        audioSource.volume = initialVolume;
    }

    private void PlayMusic(AudioSource audioSource)
    {
        if (audioSource == null)
        {
            return;
        }
        
        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
        audioSource.time = 0;
        audioSource.Play();
    }

    private void StopMusic(AudioSource audioSource)
    {
        if (audioSource == null)
        {
            return;
        }

        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }

    private IEnumerator StopMusicAfterTime(AudioSource audioSource, float delay)
    {
        yield return new WaitForSeconds(delay);
        StopMusic(audioSource);
    }

}
