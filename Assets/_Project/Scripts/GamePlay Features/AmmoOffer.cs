using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoOffer : MonoBehaviour
{
    [SerializeField]
    private int ammoCount = 0;
    [SerializeField]
    private int price = 0;
    [SerializeField]
    private int waitingTimeMin = 15;
    [SerializeField]
    private int waitingTimeMax = 25;
    [SerializeField]
    private RangedWeaponController rangedWeaponController = null;
    [SerializeField]
    private ScoreCounter scoreCounter = null;

    private System.Random waitingTimeRandom = new System.Random();

    private Coroutine offerTimerCoroutine;

    public int AmmoCount => ammoCount;
    public int Price => price;

    private event Action OfferTimeHasCome;
    
    public void RunOfferGenerator()
    {
        if (offerTimerCoroutine == null)
        {
            offerTimerCoroutine = StartCoroutine(RunNextOfferTimer());
        }      
    }
    public void StopOfferGenerator()
    {
        if (offerTimerCoroutine != null)
        {
            StopCoroutine(offerTimerCoroutine);
            offerTimerCoroutine = null;
        }
        
    }

    public void BuyAmmo(Action successCallback, Action failCallback)
    {
        if (rangedWeaponController == null)
        {
            return;
        }
        
        if (scoreCounter.TotalScoreCount >= price)
        {
            rangedWeaponController.AddBullets(ammoCount);
            scoreCounter.SubtractScore(price);
            successCallback?.Invoke();

            StartCoroutine(RunNextOfferTimer());
        }
        else
        {
            failCallback?.Invoke();
        }
    }

    public void RegisterOfferTimeHasComeListener(Action listener)
    {
        OfferTimeHasCome += listener;
    }
    public void RemoveOfferTimeHasComeListener(Action listener)
    {
        OfferTimeHasCome -= listener;
    }

    private IEnumerator RunNextOfferTimer()
    {
        int waitingTime = waitingTimeRandom.Next(waitingTimeMin, waitingTimeMax + 1);

        yield return new WaitForSeconds(waitingTime);

        OfferTimeHasCome?.Invoke();

        offerTimerCoroutine = null;
    }
}
