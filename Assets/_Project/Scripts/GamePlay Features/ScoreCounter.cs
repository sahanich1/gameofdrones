using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCounter : MonoBehaviour
{
    [SerializeField]
    private WaveController waveController = null;

    public int TotalScoreCount { get; private set; }

    private event Action ScoreCountChanged;

    public void SetScoreCount(int scoreCount)
    {
        TotalScoreCount = scoreCount;
        ScoreCountChanged?.Invoke();
    }
    public void AddScore(int scoreCount)
    {
        SetScoreCount(TotalScoreCount + scoreCount);
    }
    public void SubtractScore(int scoreCount)
    {
        SetScoreCount(TotalScoreCount - scoreCount);
    }

    public void RegisterScoreCountChangedListener(Action listener)
    {
        ScoreCountChanged += listener;
    }
    public void RemoveScoreCountChangedListener(Action listener)
    {
        ScoreCountChanged -= listener;
    }

    private void Start()
    {
        TotalScoreCount = 0;
        waveController.RegisterUnitKilledListener(OnUnitKilled);
    }

    private void OnUnitKilled(WaveUnitData waveUnitData)
    {
        AddScore(waveUnitData.ScoreForKill);
    }
}
