using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu]
public class RangedWeaponModel : WeaponModel, IRangedWeaponModel
{
    [SerializeField]
    private int initialBulletCount = 100;
    [SerializeField]
    private int clipCapacity = 10;
    [SerializeField]
    private float bulletSpeed = 200;
    [SerializeField]
    private float reloadTime = 2;

    public int ClipCapacity => clipCapacity;
    public float BulletSpeed => bulletSpeed;

    public int BulletsInStockCount { get; protected set; }
    public int LoadedBulletsCount { get; protected set; }
    public float ReloadTime => reloadTime;

    private event Action BulletCountChanged;

    public override WeaponStatus GetWeaponStatus()
    {
        if (LoadedBulletsCount <= 0)
        {
            return WeaponStatus.Unloaded;
        }
        return WeaponStatus.Ready;
    }

    public void Initialize()
    {
        LoadedBulletsCount = 0;
        BulletsInStockCount = initialBulletCount;
    }

    public void Shoot()
    {
        if (LoadedBulletsCount > 0)
        {
            LoadedBulletsCount--;
            NotifyBulletCountHasChanged();
        }
    }

    public bool CanInitiateReload()
    {
        return LoadedBulletsCount < ClipCapacity && BulletsInStockCount > 0;
    }

    public void Reload()
    {
        int bulletsToLoad = ClipCapacity - LoadedBulletsCount;
        if (bulletsToLoad == 0)
        {
            return;
        }
        if (bulletsToLoad > BulletsInStockCount)
        {
            bulletsToLoad = BulletsInStockCount;
        }
        LoadedBulletsCount += bulletsToLoad;
        BulletsInStockCount -= bulletsToLoad;

        NotifyBulletCountHasChanged();
    }
    public void AddBullets(int bulletsToAddCount)
    {
        BulletsInStockCount += bulletsToAddCount;
        NotifyBulletCountHasChanged();
    }

    public void RegisterBulletCountChangedListener(Action listener)
    {
        BulletCountChanged += listener;
    }
    public void RemoveBulletCountChangedListener(Action listener)
    {
        BulletCountChanged -= listener;
    }

    private void NotifyBulletCountHasChanged()
    {
        BulletCountChanged?.Invoke();
    }
}