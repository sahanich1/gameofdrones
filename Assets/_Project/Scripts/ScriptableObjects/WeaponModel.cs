using UnityEngine;

[CreateAssetMenu]
public abstract class WeaponModel : ScriptableObject, IWeaponModel
{
    [SerializeField]
    private float damagePerHit = 20;
    [SerializeField]
    private float attackMinDistance = 0;
    [SerializeField]
    private float attackMaxDistance = 20;
    [SerializeField]
    protected float attackHeatUpTime = 0f;
    [SerializeField]
    protected float attackCoolDownTime = 0.5f;
    [SerializeField]
    protected float attackDuration = 0.5f;
    [SerializeField]
    protected int hitsPerAttack = 1;
    [SerializeField]
    protected bool allowMovingAttack = true;
    [SerializeField]
    protected bool allowRotatingAttack = true;

    public float DamagePerHit => damagePerHit;
    public float AttackMinDistance => attackMinDistance;
    public float AttackMaxDistance => attackMaxDistance;
    public float AttackHeatUpTime => attackHeatUpTime;
    public float AttackCoolDownTime => attackCoolDownTime;
    public float AttackDuration => attackDuration;
    public int HitsPerAttack => hitsPerAttack;
    public float OneHitDuration => HitsPerAttack > 0 ? AttackDuration / HitsPerAttack : 0;
    public float DamagePerSecond => OneHitDuration > 0 ? DamagePerHit / OneHitDuration : 0;
    public bool AllowMovingAttack => allowMovingAttack;
    public bool AllowRotatingAttack => allowRotatingAttack;

    public virtual WeaponStatus GetWeaponStatus()
    {
        return WeaponStatus.Ready;
    }
}