﻿public enum WeaponStatus
{
    Ready,
    Unloaded,
    Busy
}

public enum AttackCheckResult
{
    Ok,
    WeaponIsBusy,
    AttackInProgress,
    TooShortDistance,
    TooLongDistance,
    NoTarget,
    NoWeapon,
    BadWeaponStatus
}
