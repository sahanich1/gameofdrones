using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu]
public class ActorModel : ScriptableObject
{
    [SerializeField]
    protected float initialHealth = 100;
    [SerializeField]
    protected float velocity = 10;

    protected float currentHealth;

    private event Action HealthChanged;

    public void SetHealth(float health)
    {
        if (health < 0)
        {
            health = 0;
        }
        if (this.currentHealth != health)
        {
            this.currentHealth = health;
            HealthChanged?.Invoke();
        }
    }
    public float GetHealth()
    {
        return currentHealth;
    }
    public float GetMaxHealth()
    {
        return initialHealth;
    }
    public void SetVelocity(float velocity)
    {
        this.velocity = velocity;
    }
    public float GetVelocity()
    {
        return velocity;
    }
    public bool IsAlive()
    {
        return GetHealth() > 0;
    }
    public void RegisterHealthChangedListener(Action listener)
    {
        HealthChanged += listener;
    }
    public void RemoveHealthChangedListener(Action listener)
    {
        HealthChanged -= listener;
    }

    private void Awake()
    {
        currentHealth = initialHealth;
    }
}
