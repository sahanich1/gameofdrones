using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ActorController), typeof(Animator))]
public class AnimationBehaviour : MonoBehaviour
{
    protected const float TransitionTimeBetweenLayers = 0.2f;
    protected const float ParameterChangeValueVelocity = 0.1f;
    protected const float ParameterChangeValueAccuracy = 0.01f;

    [SerializeField]
    protected float MaxVelocityForMovementBlendTree = 30f;

    protected ActorController actor = null;
    protected Animator animator = null;
    protected Coroutine animationCoroutine = null;

    public virtual void AnimateMove(Vector3 movementDirection, Vector3 previousForwardDirection)
    {
    }
    public virtual void AnimateAttack(float time)
    {
    }
    public virtual void AnimateReload(float time)
    {
    }

    public virtual void AnimateDeath()
    {
        if (IsParamaterExists(Constants.AnimatorParameters.DeathTrigger))
        {
            animator.SetTrigger(Constants.AnimatorParameters.DeathTrigger);
        }
    }

    protected void OnActorHasMoved(Vector3 movementDirection, Vector3 previousForwardDirection)
    {
        AnimateMove(movementDirection, previousForwardDirection);
    }
    protected void OnActorAttacking(float time)
    {
        AnimateAttack(time);
    }
    protected void OnActorReloading(float time)
    {
        AnimateReload(time);
    }
    protected void OnActorDamageing()
    {
        if (!actor.IsAlive())
        {
            AnimateDeath();
        }       
    }

    protected virtual void Start()
    {
        animator = GetComponent<Animator>();
        actor = GetComponent<ActorController>();

        if (IsParamaterExists(Constants.AnimatorParameters.VelocityMultiplier))
        {
            animator.SetFloat(Constants.AnimatorParameters.VelocityMultiplier, 
                actor.Velocity / MaxVelocityForMovementBlendTree);
        }

        actor.RegisterHasMovedListener(OnActorHasMoved);
        actor.RegisterAttackingListener(OnActorAttacking);
        actor.RegisterReloadingListener(OnActorReloading);
        actor.RegisterHealthChangedListener(OnActorDamageing);
    }

    protected void SetParameterValueSmooth(string paramName, float paramValue)
    {
        if (animator == null || !IsParamaterExists(paramName))
        {
            return;
        }

        float currentValue = animator.GetFloat(paramName);
        int nearestIntValue = Mathf.RoundToInt(currentValue);
        if (Mathf.Abs(currentValue - nearestIntValue) < ParameterChangeValueAccuracy
             && paramValue == nearestIntValue)
        {
            animator.SetFloat(paramName, nearestIntValue);
        }
        else
        {
            animator.SetFloat(paramName, paramValue, ParameterChangeValueVelocity, Time.deltaTime);
        }
    }

    protected void SetParameterValue(string paramName, float paramValue)
    {
        if (animator == null || !IsParamaterExists(paramName))
        {
            return;
        }

        animator.SetFloat(paramName, paramValue);

    }

    protected IEnumerator AnimateAction(string parameter, float actionTime)
    {
        if (animator == null || actionTime <= 0 || !IsParamaterExists(parameter))
        {
            animationCoroutine = null;
            yield break;
        }

        animator.SetBool(parameter, true);
        yield return new WaitForSeconds(actionTime);
        animator.SetBool(parameter, false);

        animationCoroutine = null;
    }

    protected IEnumerator AnimateLayerAction(string layerName, string parameter, float actionTime)
    {
        if (animator == null || !IsLayerExists(layerName) || !IsParamaterExists(parameter))
        {
            animationCoroutine = null;
            yield break;
        }

        if (actionTime <= 0)
        {
            animationCoroutine = null;
            yield break;
        }

        int layerIndex = animator.GetLayerIndex(layerName);

        animator.SetBool(parameter, true);

        float currentTransitionTime = 0;
        while (currentTransitionTime < TransitionTimeBetweenLayers)
        {
            animator.SetLayerWeight(layerIndex, currentTransitionTime / TransitionTimeBetweenLayers);
            yield return null;
            currentTransitionTime += Time.deltaTime;
        }
        animator.SetLayerWeight(layerIndex, 1);

        yield return new WaitForSeconds(actionTime - 2 * TransitionTimeBetweenLayers);

        currentTransitionTime = 0;
        while (currentTransitionTime < TransitionTimeBetweenLayers)
        {
            animator.SetLayerWeight(layerIndex, 1 - currentTransitionTime / TransitionTimeBetweenLayers);
            yield return null;
            currentTransitionTime += Time.deltaTime;
        }
        
        animator.SetLayerWeight(layerIndex, 0);
        animator.SetBool(parameter, false);

        animationCoroutine = null;
    }

    protected bool IsParamaterExists(string paramName)
    {
        for (int i = 0; i < animator.parameterCount; i++)
        {
            if (animator.parameters[i].name == paramName)
            {
                return true;
            }
        }
        return false;
    }

    protected bool IsLayerExists(string paramName)
    {
        for (int i = 0; i < animator.layerCount; i++)
        {
            if (animator.GetLayerName(i) == paramName)
            {
                return true;
            }
        }
        return false;
    }

    private void OnDestroy()
    {
        actor.RemoveHasMovedListener(OnActorHasMoved);
        actor.RemoveAttackingListener(OnActorAttacking);
        actor.RemoveReloadingListener(OnActorReloading);
        actor.RemoveHealthChangedListener(OnActorDamageing);
    }
}


