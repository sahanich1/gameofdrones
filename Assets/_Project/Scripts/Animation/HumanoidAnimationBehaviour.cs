using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanoidAnimationBehaviour : AnimationBehaviour
{
    private const float SideVelocityWhenRotating = 0.5f;

    public override void AnimateMove(Vector3 movementDirection, Vector3 previousForwardDirection)
    {
        if (animator == null)
        {
            return;
        }

        float velocityZ = 0;
        float velocityX = 0;
        if (movementDirection != Vector3.zero)
        {
            velocityZ = Vector3.Dot(movementDirection.normalized, transform.forward);
            velocityX = Vector3.Dot(movementDirection.normalized, transform.right);
        }
        else if (previousForwardDirection != transform.forward)
        {
            float rotationAngle = Vector3.SignedAngle(previousForwardDirection, transform.forward, Vector3.up);
            velocityX = rotationAngle > 0 ? SideVelocityWhenRotating : -SideVelocityWhenRotating;
        }

        SetParameterValueSmooth(Constants.AnimatorParameters.ForwardVelocity, velocityZ);
        SetParameterValueSmooth(Constants.AnimatorParameters.SideVelocity, velocityX);

        if (!IsParamaterExists(Constants.AnimatorParameters.Moving))
        {
            return;
        }

        bool moving = animator.GetBool(Constants.AnimatorParameters.Moving);
        bool change = moving && movementDirection == Vector3.zero || !moving && movementDirection != Vector3.zero;
        if (change)
        {
            animator.SetBool(Constants.AnimatorParameters.Moving, movementDirection != Vector3.zero);
        }
    }

    public override void AnimateAttack(float time)
    {
        if (animationCoroutine == null)
        {
            animationCoroutine = StartCoroutine(AnimateAction(Constants.AnimatorParameters.Attacking, time));
        }
    }
    public override void AnimateReload(float time)
    {
        if (animationCoroutine == null)
        {
            animationCoroutine = StartCoroutine(
                AnimateLayerAction(Constants.AnimatorLayers.UpperBody, Constants.AnimatorParameters.Reloading, time));
        }
    }
}

