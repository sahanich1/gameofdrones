using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WeaponController : MonoBehaviour, IWeaponController
{
    protected Coroutine longOperationCoroutine = null;
    protected IDamageable target = null;
    protected IWeaponModel model = null;
    protected Transform weaponAttackPoint = null;

    protected GameObject owner = null;

    public Transform WeaponAttackPoint => weaponAttackPoint;
    public bool AllowMovingAttack => model.AllowMovingAttack;
    public bool AllowRotatingAttack => model.AllowRotatingAttack;
    public float AttackMinDistance => model.AttackMinDistance;
    public float AttackMaxDistance => model.AttackMaxDistance;
    public float FullAttackTime => model.AttackDuration + model.AttackHeatUpTime + model.AttackCoolDownTime;
    public bool IsAttacking { get; protected set; }  = false;

    private event Action<IDamageable> TargetChanged;

    public WeaponStatus GetWeaponStatus()
    {
        return model.GetWeaponStatus();
    }
    public void SetTarget(IDamageable target)
    {
        this.target = target;
        TargetChanged?.Invoke(target);
    }
    public void RegisterTargetChangedListener(Action<IDamageable> listener)
    {
        TargetChanged += listener;
    }
    public void RemoveTargetChangedListener(Action<IDamageable> listener)
    {
        TargetChanged -= listener;
    }

    public void InterruptCurrentOperation()
    {
        if (longOperationCoroutine != null)
        {
            StopCoroutine(longOperationCoroutine);
            longOperationCoroutine = null;
        }
    }

    public AttackCheckResult CheckTargetAttackDistance()
    {
        if (target == null)
        {
            return AttackCheckResult.NoTarget;
        }

        float distanceToTarget = Vector3.Distance(weaponAttackPoint.position, target.GetPosition());
        if (distanceToTarget <= model.AttackMinDistance)
        {
            return AttackCheckResult.TooShortDistance;
        }
        else if (distanceToTarget > model.AttackMaxDistance)
        {
            return AttackCheckResult.TooLongDistance;
        }
        
        return AttackCheckResult.Ok;
    }

    public AttackCheckResult CheckTargetAttackPossibility()
    {
        if (GetWeaponStatus() != WeaponStatus.Ready)
        {
            return AttackCheckResult.BadWeaponStatus;
        }

        return CheckTargetAttackDistance();
    }

    public AttackCheckResult AttackTarget()
    {
        if (IsBusy())
        {
            return AttackCheckResult.WeaponIsBusy;
        }

        AttackCheckResult result = CheckTargetAttackPossibility();

        if (result == AttackCheckResult.Ok)
        {
            longOperationCoroutine = StartCoroutine(PerformAttack());
        }

        return result;
    }

    public AttackCheckResult ForcedAttack()
    {
        if (IsBusy())
        {
            return AttackCheckResult.WeaponIsBusy;
        }
        if (GetWeaponStatus() != WeaponStatus.Ready)
        {
            return AttackCheckResult.BadWeaponStatus;
        }
        longOperationCoroutine = StartCoroutine(PerformAttack());
        return AttackCheckResult.Ok;
    }

    public bool IsBusy()
    {
        return longOperationCoroutine != null;
    }

    public virtual void SetOwner(GameObject owner)
    {
        this.owner = owner;
    }

    protected virtual void Hit()
    {
    }

    protected virtual void Start()
    {
    }

    protected virtual void Awake()
    {
    }

    private IEnumerator PerformAttack()
    {
        IsAttacking = true;
        if (model.AttackHeatUpTime > 0)
        {
            yield return new WaitForSeconds(model.AttackHeatUpTime);
        }

        for (int i = 0; i < model.HitsPerAttack; i++)
        {
            Hit();
            yield return new WaitForSeconds(model.OneHitDuration);
        }

        if (model.AttackCoolDownTime > 0)
        {
            yield return new WaitForSeconds(model.AttackCoolDownTime);
        }
        longOperationCoroutine = null;
        IsAttacking = false;
    }


}
