using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedWeaponController : WeaponController, IRangedWeaponController
{
    [SerializeField]
    protected RangedWeaponModel rangedWeaponModel = null;
    [SerializeField]
    protected Shot bulletPrefab = null;
    [SerializeField]
    protected Transform bulletSpawnPoint = null;

    protected float bulletHalfYSize = 0;
    public bool IsReloading { get; protected set; } = false;
    public float ReloadTime => rangedWeaponModel.ReloadTime;
    public int BulletsInStockCount => rangedWeaponModel.BulletsInStockCount;
    public int LoadedBulletsCount => rangedWeaponModel.LoadedBulletsCount;

    protected override void Awake()
    {
        base.Awake();
        weaponAttackPoint = bulletSpawnPoint;
        rangedWeaponModel = Instantiate(rangedWeaponModel, transform);
        rangedWeaponModel.Initialize();
        model = rangedWeaponModel;
    }

    protected override void Start()
    {
        base.Start();

        Shot bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
        Collider bulletCollider = bullet.GetComponent<Collider>();
        bulletHalfYSize = bulletCollider.bounds.size.y / 2;
        Destroy(bullet.gameObject);
    }

    protected override void Hit()
    {
        if (!IsItPossibleToShoot())
        {
            return;
        }

        PerformShoot(GetShootPosition(), GetShootDirection());
        rangedWeaponModel.Shoot();
    }

    public void AddBullets(int bulletsToAddCount)
    {
        rangedWeaponModel.AddBullets(bulletsToAddCount);
    }
    public bool CanReload()
    {
        return rangedWeaponModel.CanInitiateReload();
    }
    public void Reload()
    {
        if (CanReload() && !IsBusy())
        {
            longOperationCoroutine = StartCoroutine(PerformReload());
        }      
    }

    public void RegisterBulletCountChangedListener(Action listener)
    {
        rangedWeaponModel.RegisterBulletCountChangedListener(listener);
    }
    public void RemoveBulletCountChangedListener(Action listener)
    {
        rangedWeaponModel.RemoveBulletCountChangedListener(listener);
    }

    private IEnumerator PerformReload()
    {
        IsReloading = true;
        if (rangedWeaponModel.ReloadTime > 0)
        {
            yield return new WaitForSeconds(rangedWeaponModel.ReloadTime);
        }

        rangedWeaponModel.Reload();

        longOperationCoroutine = null;
        IsReloading = false;
    }

    protected bool IsItPossibleToShoot()
    {
        if (bulletPrefab == null)
        {
            Debug.LogWarning($"{name}: {bulletPrefab.name} is null");
            Debug.Break();
            return false;
        }
        if (rangedWeaponModel.GetWeaponStatus() == WeaponStatus.Unloaded)
        {
            return false;
        }
        return true;
    }

    protected void PerformShoot(Vector3 shootPosition, Vector3 shootDirection)
    {
        // �.�. ���� ����� 2D, � ������� ������������ ��� ������� �����������, ������������ �� ������ ������ ��� Y
        float rotationY = Vector3.SignedAngle(Vector3.forward, Vector3.Scale(shootDirection, new Vector3(1, 0, 1)), Vector3.up);
        Shot bullet = Instantiate(bulletPrefab, shootPosition,
             Quaternion.Euler(0, rotationY, 0));
        bullet.transform.forward = shootDirection;
        bullet.SetOwner(owner);
        bullet.SetMaxDistance(rangedWeaponModel.AttackMaxDistance);
        bullet.SetVelocity(shootDirection * rangedWeaponModel.BulletSpeed);
        bullet.SetDamage(rangedWeaponModel.DamagePerHit);      
    }

    protected Vector3 GetShootDirection()
    {
        Vector3 velocityDirection;
        Vector3 endBulletPoint;

        velocityDirection = bulletSpawnPoint.forward;
        endBulletPoint = bulletSpawnPoint.position + velocityDirection * AttackMaxDistance;

        if (target != null && target.IsAlive())
        {
            // ���� ���� ����, ������������ Y-����������, ����� ������� �� ��� ���� 
            // � ������ �������������� ����� ����� �������� � ����
            velocityDirection.y = (target.GetPosition() - bulletSpawnPoint.position).normalized.y;
            velocityDirection.Normalize();
            endBulletPoint = bulletSpawnPoint.position + velocityDirection * 
                Vector3.Distance(bulletSpawnPoint.position, target.GetPosition());
        }

        // ���� ������ �������, �� ����� ��������� ����� � ���������� �� ����, ��� ��������� ����. 
        // ���������, ��� ���� ��� ���, �� ����������� ������ �������� ����� ����� �� �������� ������ �������. 
        if (endBulletPoint.y - bulletHalfYSize <= GameManager.Instance.GroundTopY)
        {
            endBulletPoint.y = GameManager.Instance.GroundTopY + bulletHalfYSize * 1.01f;
            velocityDirection = (endBulletPoint - bulletSpawnPoint.position).normalized;
        }

        return velocityDirection;
    }

    protected Vector3 GetShootPosition()
    {
        return bulletSpawnPoint.position;
    }

}
