using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider)), RequireComponent(typeof(Rigidbody))]
public class Shot : MonoBehaviour
{
    [SerializeField]
    private float damage = 10;

    private Vector3 startPoint = Vector3.zero;
    private float maxDistance = 0;

    private Rigidbody rb = null;
    private GameObject owner = null;

    public event Action BeforeDestroyWithoutHit;
    public event Action HittedWithDamage;

    public void SetDamage(float damage)
    {
        this.damage = damage;
    }
    public void SetVelocity(Vector3 velocityVector)
    {
        rb.velocity = Vector3.zero;
        rb.AddForce(velocityVector, ForceMode.Impulse);
    }
    public void SetMaxDistance(float maxDistance)
    {
        this.maxDistance = maxDistance;
    }
    public void SetOwner(GameObject owner)
    {
        this.owner = owner;
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        startPoint = transform.position;
    }

    private void Update()
    {
        if (Vector3.Distance(startPoint, transform.position) > maxDistance)
        {
            rb.velocity = Vector3.zero;
            BeforeDestroyWithoutHit?.Invoke();
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Friendly fire � ���� ���� �� �����
        if (owner != null && other.gameObject.layer == owner.layer)
        {
            return;
        }
        if (other.GetComponent<Shot>() != null)
        {
            return;
        }
        
        IDamageable target = other.GetComponent<IDamageable>();
        if (target != null)
        {
            target.ReceiveDamage(damage);
            HittedWithDamage?.Invoke();
        }
        else
        {
            BeforeDestroyWithoutHit?.Invoke();
        }
        Destroy(gameObject);

    }
    private void OnCollisionEnter(Collision collision)
    {
        OnTriggerEnter(collision.collider);
    }
}