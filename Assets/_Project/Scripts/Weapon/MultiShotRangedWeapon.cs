using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiShotRangedWeapon : RangedWeaponController
{
    [SerializeField]
    private float shotCount = 0;
    [SerializeField]
    private float shotDispersionAngle = 0;
    [SerializeField]
    private float shotSpawnAreaWidth = 0;

    protected override void Hit()
    {
        if (!IsItPossibleToShoot() || shotCount <= 0)
        {
            return;
        }

        // ������� ����� ����������� ������. ������������ ��������� ����� �� ������ ��������� ���� ��������
        // � ������ ���� ��������.

        Vector3 centralShotDirection = GetShootDirection();
        Vector3 centralShotPosition = GetShootPosition();

        float angleBetweenShots = shotDispersionAngle / (shotCount - 1);
        float distanceBetweenShots = shotSpawnAreaWidth / (shotCount - 1);

        float leftShotAngle = -shotDispersionAngle / 2;
        Vector3 leftShotPosition = centralShotPosition - bulletSpawnPoint.right * shotSpawnAreaWidth / 2;

        for (int i = 0; i < shotCount; i++)
        {
            Vector3 currentShotDirection = Quaternion.Euler(0, leftShotAngle + i * angleBetweenShots, 0) * centralShotDirection;
            Vector3 currentShotPosition = leftShotPosition + bulletSpawnPoint.right * i * distanceBetweenShots;
            PerformShoot(currentShotPosition, currentShotDirection);
        }

        // ��� ������ ������������� ������� ����������� ��������� ��������� �� ����
        rangedWeaponModel.Shoot();
    }
}
