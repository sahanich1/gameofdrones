﻿using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

[Serializable]
public class EffectData
{
    [SerializeField]
    private GameObject effectObject = null;
    [SerializeField]
    private float duration = 1;

    private ParticleSystem particleSystem = null;
    private AudioSource audioSource = null;

    private bool initialized = false;

    public GameObject EffectObject => effectObject;
    public float Duration => duration;

    public bool IsPrefab => effectObject != null && effectObject.scene.name == null;
    public bool IsPlayable => effectObject != null && duration > 0;

    public void Initialize()
    {
        if (initialized)
        {
            return;
        }
        
        if (effectObject == null)
        {
            return;
        }
        
        if (IsPrefab)
        {
            Debug.LogWarning($"{Constants.Messages.NoPrefab} {effectObject}");
            return;
        }

        particleSystem = effectObject.GetComponent<ParticleSystem>();
        audioSource = effectObject.GetComponent<AudioSource>();
        initialized = true;
    }

    public ParticleSystem GetParticleSystem()
    {
        Initialize();              
        return particleSystem;
    } 
    
    public AudioSource GetAudioSource()
    {
        Initialize();
        return audioSource;
    }

    public void Play(Vector3 position)
    {
        Initialize();
        effectObject.transform.position = position;
        effectObject.SetActive(true);
        PlayAudioSource();
        PlayParticleSystem();
    }

    private async void PlayAudioSource()
    {
        if (audioSource == null)
        {
            return;
        }

        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }

        audioSource.Play();

        await Task.Run(() => Thread.Sleep((int)(duration * 1000f)));
        
        if (audioSource != null && audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }

    private async void PlayParticleSystem()
    {
        if (particleSystem == null)
        {
            return;
        }

        if (particleSystem.isPlaying)
        {
            particleSystem.Stop();
        }

        particleSystem.Play();

        await Task.Run(() => Thread.Sleep((int)(duration * 1000f)));

        // За время ожидания эмиссия частиц могла завершиться
        if (particleSystem != null && particleSystem.isPlaying)
        {
            particleSystem.Stop();
        }
    }
}
