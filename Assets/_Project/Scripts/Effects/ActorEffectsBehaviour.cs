using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorEffectsBehaviour : MonoBehaviour
{
    [SerializeField]
    private EffectData startAttack = null;
    [SerializeField]
    private EffectData reload = null;
    [SerializeField]
    private EffectData receiveDamage = null;
    [SerializeField]
    private EffectData death = null;

    [SerializeField]
    private GameObject[] objectsToEnableAfterDeath = null;
    [SerializeField]
    private GameObject[] objectsToDisableAfterDeath = null;
    [SerializeField]
    private Rigidbody[] objectsToBlowUpAfterDeath = null;

    private ActorController actor = null;

    private void Awake()
    {
        actor = GetComponent<ActorController>();
    }
    private void Start()
    {
        actor.RegisterHealthChangedListener(OnActorHealthChanged);
        actor.RegisterAttackingListener((x) => OnActorAttacking());
        actor.RegisterReloadingListener((x) => OnActorReloading());
    }
    private void OnDestroy()
    {
        actor.RemoveHealthChangedListener(OnActorHealthChanged);
        actor.RemoveAttackingListener((x) => OnActorAttacking());
        actor.RemoveReloadingListener((x) => OnActorReloading());
    }
    private void OnActorHealthChanged()
    {
        if (actor.IsAlive())
        {
            ActivateEffect(receiveDamage);
        }
        else
        {
            ActivateEffect(death);
            foreach (var item in objectsToEnableAfterDeath)
            {
                item.SetActive(true);
            }
            
            foreach (var item in objectsToDisableAfterDeath)
            {
                item.SetActive(false);
            }

            foreach (var item in objectsToBlowUpAfterDeath)
            {
                item.AddExplosionForce(2, transform.position, 3, 1, ForceMode.Impulse);
            }
        }
        
    }
    private void OnActorAttacking()
    {
        ActivateEffect(startAttack);
    }
    private void OnActorReloading()
    {
        ActivateEffect(reload);
    }
    private void ActivateEffect(EffectData effectData)
    {
        if (effectData == null || !effectData.IsPlayable)
        {
            return;
        }
        effectData.Play(transform.position);
    }
}
