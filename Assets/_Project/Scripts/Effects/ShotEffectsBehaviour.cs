using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotEffectsBehaviour : MonoBehaviour
{
    [SerializeField]
    private EffectData instantiate = null;
    [SerializeField]
    private EffectData destroyWithoutHit = null;
    [SerializeField]
    private EffectData hitWithDamage = null;

    private Shot shot = null;

    private void Awake()
    {
        shot = GetComponent<Shot>();
    }
    private void Start()
    {
        shot.HittedWithDamage += OnShotHittedWithDamage;
        shot.BeforeDestroyWithoutHit += OnShotBeforeDestroyWithoutHit;
        ActivateEffect(instantiate);
    }

    private void OnShotBeforeDestroyWithoutHit()
    {
        ActivateEffect(destroyWithoutHit);
    }

    private void OnShotHittedWithDamage()
    {
        ActivateEffect(hitWithDamage);
    }

    private void ActivateEffect(EffectData effectData)
    {
        if (effectData == null || !effectData.IsPlayable)
        {
            return;
        }

        GameObject gameObject = Instantiate(effectData.EffectObject, transform.position, Quaternion.identity);
        Destroy(gameObject, effectData.Duration);
    }
}
