﻿using UnityEngine;

public interface IWeaponController
{
    bool AllowMovingAttack { get; }
    bool AllowRotatingAttack { get; }
    float AttackMinDistance { get; }
    float AttackMaxDistance { get; }
    float FullAttackTime { get; }
    bool IsAttacking { get; }
    void SetOwner(GameObject owner);
    WeaponStatus GetWeaponStatus();
    void SetTarget(IDamageable target);
    AttackCheckResult CheckTargetAttackDistance();
    AttackCheckResult AttackTarget();
    AttackCheckResult ForcedAttack();
    bool IsBusy();
}
