﻿using System;

public interface IRangedWeaponController: IWeaponController
{
    int BulletsInStockCount { get; }
    int LoadedBulletsCount { get; }
    bool IsReloading { get; }
    float ReloadTime { get; }
    void AddBullets(int bulletsToAddCount);
    void Reload();
    void RegisterBulletCountChangedListener(Action listener);
    void RemoveBulletCountChangedListener(Action listener);
}
