using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    float Health { get; }
    float MaxHealth { get; }
    Vector3 GetPosition();
    bool IsAlive();
    void ReceiveDamage(float damage);
    void RegisterHealthChangedListener(Action listener);
    void RemoveHealthChangedListener(Action listener);
}
