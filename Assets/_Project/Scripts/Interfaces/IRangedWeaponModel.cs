﻿using System;

public interface IRangedWeaponModel: IWeaponModel
{
    int ClipCapacity { get; }
    float BulletSpeed { get; }
    float ReloadTime { get; }
    int BulletsInStockCount { get; }
    int LoadedBulletsCount { get; }
    void Initialize();
    void AddBullets(int bulletsToAddCount);
    void Shoot();
    bool CanInitiateReload();
    void Reload();
    void RegisterBulletCountChangedListener(Action listener);
    void RemoveBulletCountChangedListener(Action listener);
}
