﻿public interface IWeaponModel
{
    float DamagePerHit { get; }
    float DamagePerSecond { get; }
    float AttackMinDistance { get; }
    float AttackMaxDistance { get; }
    float AttackHeatUpTime { get; }
    float AttackCoolDownTime { get; }
    float AttackDuration { get; }
    int HitsPerAttack { get; }
    bool AllowMovingAttack { get; }
    bool AllowRotatingAttack { get; }
    public float OneHitDuration { get; }
    WeaponStatus GetWeaponStatus();
}
