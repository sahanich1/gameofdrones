﻿public interface ILimitedCounter<T>
{
    T GetObject();
    void SetLimit(int limit);
    void ResetCounter();
    void IncreaseCounter();
    bool IsLimitReached();
}
