using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour
{
    [SerializeField]
    ScoreCounter scoreCounter = null;
    [SerializeField]
    Text scoreCountText = null;

    private void Start()
    {
        scoreCounter.RegisterScoreCountChangedListener(OnScoreCountChanged);
        OnScoreCountChanged();
    }
    private void OnDestroy()
    {
        scoreCounter.RemoveScoreCountChangedListener(OnScoreCountChanged);
    }

    private void OnScoreCountChanged()
    {
        scoreCountText.text = $"{scoreCounter.TotalScoreCount} {Constants.UI.ScoreUnit}";
    }
}
