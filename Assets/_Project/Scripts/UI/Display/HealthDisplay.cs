using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour
{
    [SerializeField]
    ActorController actorController = null;
    [SerializeField]
    Text healthCountText = null;

    private void Start()
    {
        actorController.RegisterHealthChangedListener(OnHealthChanged);
        OnHealthChanged();
    }

    private void OnHealthChanged()
    {
        healthCountText.text = actorController.Health.ToString();
    }

}
