using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyStatusDisplay : MonoBehaviour
{
    [SerializeField]
    private WeaponController playerWeaponController = null;
    [SerializeField]
    private Text currentHealthText = null;
    [SerializeField]
    private Text maxHealthText = null;

    private IDamageable currentTarget = null;

    private void Start()
    {
        gameObject.SetActive(false);
        playerWeaponController.RegisterTargetChangedListener(OnWeaponTargetChanged);
    }

    private void OnDestroy()
    {
        playerWeaponController.RemoveTargetChangedListener(OnWeaponTargetChanged);
    }

    private void OnWeaponTargetChanged(IDamageable target)
    {
        if (target == null)
        {
            gameObject.SetActive(false);
            if (currentTarget != null)
            {
                currentTarget.RemoveHealthChangedListener(OnTargetHealthChanged);                
            }                      
            return;
        }

        target.RegisterHealthChangedListener(OnTargetHealthChanged);
        currentTarget = target;

        OnTargetHealthChanged();
        gameObject.SetActive(true);
    }

    private void OnTargetHealthChanged()
    {
        currentHealthText.text = currentTarget.Health.ToString();
        maxHealthText.text = currentTarget.MaxHealth.ToString();
    }
}
