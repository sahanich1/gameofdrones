using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoDisplay : MonoBehaviour
{
    [SerializeField]
    private ActorController actorController = null;
    [SerializeField]
    private Text ammoInStockCountText = null;
    [SerializeField]
    private Text loadedAmmoCountText = null;

    private IRangedWeaponController rangedWeapon = null;

    private void Start()
    {
        if (CommonFunctions.GetInterface(actorController.WeaponController, out rangedWeapon))
        {
            rangedWeapon.RegisterBulletCountChangedListener(OnWeaponBulletCountChanged);
            OnWeaponBulletCountChanged();
        }
    }

    private void OnDestroy()
    {
        rangedWeapon.RemoveBulletCountChangedListener(OnWeaponBulletCountChanged);
    }

    private void OnWeaponBulletCountChanged()
    {
        ammoInStockCountText.text = rangedWeapon.BulletsInStockCount.ToString();
        loadedAmmoCountText.text = rangedWeapon.LoadedBulletsCount.ToString();
    }
}
