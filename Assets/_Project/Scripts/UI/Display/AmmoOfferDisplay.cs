using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoOfferDisplay : MonoBehaviour
{
    [SerializeField]
    private AmmoOffer ammoOffer = null;
    [SerializeField]
    private Text ammoCountText = null;
    [SerializeField]
    private Text priceText = null;
    [SerializeField]
    private Button buyAmmoButton = null;

    public void BuyAmmo()
    {
        ammoOffer.BuyAmmo(OnBuyAmmoSuccess, OnBuyAmmoFail);
    }

    private void Start()
    {
        ammoCountText.text = ammoOffer.AmmoCount.ToString();
        priceText.text = $"{ammoOffer.Price} {Constants.UI.ScoreUnit}";
        buyAmmoButton.onClick.AddListener(() => BuyAmmo());

        ammoOffer.RegisterOfferTimeHasComeListener(OnOfferTimeHasCome);

        gameObject.SetActive(false);
    }

    private void OnOfferTimeHasCome()
    {
        gameObject.SetActive(true);
    }
    private void OnBuyAmmoSuccess()
    {
        gameObject.SetActive(false);
    }
    private void OnBuyAmmoFail()
    {
    }

}
