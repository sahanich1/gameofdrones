using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour
{
    [SerializeField]
    private Text scoreCountText = null;
    [SerializeField]
    private Button restartButton = null;
    [SerializeField]
    private Button quitButton = null;

    public void Show(int scoreCount)
    {
        scoreCountText.text = $"{scoreCount} {Constants.UI.ScoreUnit}";
        gameObject.SetActive(true);
    }

    private void Start()
    {
        restartButton.onClick.AddListener(() => GameManager.RunGame());
        quitButton.onClick.AddListener(() => GameManager.Quit());
    }
}
