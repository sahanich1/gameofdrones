using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveCompletedScreen : MonoBehaviour
{
    [SerializeField]
    private Text waveInfoText = null;
    [SerializeField]
    private int displayTime = 0;

    public void Show(int waveIndex, Action callback)
    {
        waveInfoText.text = string.Format(Constants.Messages.WaveCompleted, waveIndex + 1);
        gameObject.SetActive(true);
        StartCoroutine(HideAfterTime(callback));
    }

    private IEnumerator HideAfterTime(Action callback)
    {
        yield return new WaitForSeconds(displayTime);

        callback?.Invoke();
        gameObject.SetActive(false);
    }
}
