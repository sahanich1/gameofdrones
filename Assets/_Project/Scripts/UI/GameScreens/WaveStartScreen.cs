using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveStartScreen : MonoBehaviour
{
    private const int countdownStartNumber = 3;

    [SerializeField]
    private Text waveInfoText = null;
    [SerializeField]
    private Text countdownText = null;
    [SerializeField]
    private int displayTime = 0;

    public void Show(int waveIndex, Action callbackAfterCountdownEnd)
    {
        waveInfoText.text = $"{Constants.Messages.Wave} {waveIndex + 1}";
        gameObject.SetActive(true);
        StartCoroutine(HideAfterCountdown(callbackAfterCountdownEnd));
    }

    private IEnumerator HideAfterCountdown(Action callbackAfterCountdownEnd)
    {
        float secondsForOneNumber = displayTime / ((float)countdownStartNumber + 1);
        int currentNumber = countdownStartNumber;
        while (currentNumber > 0)
        {
            countdownText.text = $"{currentNumber}";
            yield return new WaitForSeconds(secondsForOneNumber);
            currentNumber--;
        }
        
        countdownText.text = Constants.Messages.Fight;
        yield return new WaitForSeconds(secondsForOneNumber);

        callbackAfterCountdownEnd?.Invoke();
        gameObject.SetActive(false);
    }
}
