using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenController : MonoBehaviour
{
    [SerializeField]
    private WaveStartScreen waveStartScreen = null;
    [SerializeField]
    private WaveCompletedScreen waveCompletedScreen = null;
    [SerializeField]
    private GameOverScreen gameOverScreen = null;
    [SerializeField]
    private GameOverScreen gameCompletedScreen = null;

    private List<GameObject> screens = new List<GameObject>();

    public void ShowWaveStartScreen(int waveIndex, Action callback)
    {
        SetActiveScreen(waveStartScreen.gameObject);
        waveStartScreen.Show(waveIndex, callback);
    }
    public void ShowWaveCompletedScreen(int waveIndex, Action callback)
    {
        SetActiveScreen(waveCompletedScreen.gameObject);
        waveCompletedScreen.Show(waveIndex, callback);
    }
    public void ShowGameOverScreen(int scoreCount)
    {
        SetActiveScreen(gameOverScreen.gameObject);
        gameOverScreen.Show(scoreCount);
    }
    public void ShowGameCompletedScreen(int scoreCount)
    {
        SetActiveScreen(gameCompletedScreen.gameObject);
        gameCompletedScreen.Show(scoreCount);
    }

    private void Awake()
    {
        screens.Add(waveStartScreen.gameObject);
        screens.Add(waveCompletedScreen.gameObject);
        screens.Add(gameOverScreen.gameObject);
        screens.Add(gameCompletedScreen.gameObject);
        SetActiveScreen(null);
    }

    private void SetActiveScreen(GameObject screen)
    {
        foreach (var item in screens)
        {
            item.gameObject.SetActive(item.gameObject == screen);
        }
    }

}
